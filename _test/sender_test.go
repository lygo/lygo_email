package _test

import (
	"bitbucket.org/lygo/lygo_email"
	"testing"
)

func TestSender(t *testing.T) {

	sender, err := lygo_email.NewSender("./config.json")
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	err = sender.Send("test", "<strong>HELLO MESSAGE</strong>", []string{"angelo.geminiani@botika.ai"}, "", nil)
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}


}

