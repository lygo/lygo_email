package _test

import (
	"bitbucket.org/lygo/lygo_email"
	"crypto/tls"
	"net/mail"
	"net/smtp"
	"testing"
)

func TestSimple(t *testing.T) {

	username := "xxx"
	password := "xxx"

	// compose the message
	m := lygo_email.NewMessage("Hi", "this is the body")
	m.From = &mail.Address{Name: "From", Address: "from@example.com"}
	m.AddTo(mail.Address{Name: "To", Address: "xxxx@example.com"})
	m.AddCc(mail.Address{Name: "someCcName", Address: "xxxx@example.com"})
	m.AddBcc(mail.Address{Name: "someBccName", Address: "xxxx@example.com"})

	// add attachments
	if err := m.AddAttachment("readme.md"); err != nil {
		t.Error(err)
		t.FailNow()
	}

	// add headers
	m.AddHeader("X-CUSTOMER-id", "1234567789")

	// send it
	auth := smtp.PlainAuth("", username, password, "ssl0.ovh.net")
	if err := lygo_email.Send("ssl0.ovh.net:587", auth, m); err != nil {
		t.Error(err)
		t.FailNow()
	}

}

func TestGMail(t *testing.T) {

	username := "xxx"
	password := "xxx"

	// compose the message
	m := lygo_email.NewHTMLMessage("Hi", "this <strong>is</strong> the body")
	m.From = &mail.Address{Name: "From", Address: username}
	m.AddTo(mail.Address{Name: "Gian Angelo", Address: "angelo.geminiani@gmail.com"})

	// add attachments
	if err := m.AddAttachment("../readme.md"); err != nil {
		t.Error(err)
		t.FailNow()
	}

	// add headers
	// m.AddHeader("X-CUSTOMER-id", "1234567789")

	// send it
	host := "smtp.gmail.com"
	auth := smtp.PlainAuth("", username, password, host)
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}
	if err := lygo_email.SendSecure(host+":465", auth, tlsconfig, m); err != nil {
		t.Error(err)
		t.FailNow()
	}

}

