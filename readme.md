# LyGo Email

![icon](icon.png)

Simple email helper using smtp/email

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_email@v0.1.6`

### Versioning

Sources are versioned using git tags:

```
git tag v0.1.6
git push origin v0.1.6
```

## Dependencies

No dependencies