module bitbucket.org/lygo/lygo_email

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.119
	github.com/google/uuid v1.3.0 // indirect
)
